# Open source tools for Broadcasters
This repo provides slides and prebuilt binaries for selected Raspberry Pi (Linux ARM).

Software with a \* has a binary.
Software talked about/provided:
* [OB Player/Server](https://github.com/openbroadcaster/)
* [Rivendell](https://github.com/ElvishArtisan/rivendel)
* [OnAirScreen](https://github.com/saschaludwig/OnAirScreen) \*
Note: This app has had the weather widget removed due to issues with the Pi build.
* [Open OB](https://github.com/JamesHarrison/openob)
* [Clean Feed](https://cleanfeed.net)
* [Asterisk](https://asterisk.org)
* [Free PBX](https://freepbx.org)
* [Silent Jack](https://github.com/njh/silentjack) \*
* [Jack Meter](https://github.com/njh/jackmeter) \*

# Installing prebuilt binaries

# Installing OnAirScreen

## Systems with sudo
```bash
sudo mv OnAirScreen /usr/local/bin/ && sudo chmod +x /usr/local/bin/OnAirScreen
```

## Systems without sudo
```bash
su
mv OnAirScreen /usr/local/bin/ && chmod +x /usr/local/bin/OnAirScreen
```

# Installing Jack Meter

## Systems with sudo
```bash
sudo mv jackmeter /usr/local/bin/ && sudo chmod +x /usr/local/bin/jackmeter
```

## Systems without sudo
```bash
su
mv jackmeter /usr/local/bin/ && chmod +x /usr/local/bin/jackmeter
```

[Find me online](https://bit.ly/2kEUaIZ)
["hire me"]("https://vincentmaggard.dev")
